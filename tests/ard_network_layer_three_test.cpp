
#include "ard_event_manager.h"
#include "ard_fake_network_interfaces.h"
#include "ard_mempool.h"
#include "ard_network_address.h"
#include "ard_network_layer_interface_mock.h"
#include "ard_network_layer_three.h"
#include "ard_network_layer_three_routing.h"
#include "ard_sys_interface_mock.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using ::testing::_;
using ::testing::Each;
using ::testing::ElementsAre;
using ::testing::NiceMock;
using ::testing::Invoke;

MATCHER_P(IsPktBuffEq, ref_pk, "") {
  if (*arg == ref_pk)
    return true;

  *result_listener << "Packet buffers are different\n";
  *result_listener << "The test was expecting this packet:\n";
  *result_listener << ref_pk.prettyPrint().str();
  *result_listener << "But your implementation sent this one:\n";
  *result_listener << arg->prettyPrint().str();
  return false;
}



TEST(NetworkLayerThreeTest, SendRequest) {

  ArdMemPool<ArdPktBuffer> mem_pool;

  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L3Addr src_l3addr(3);
  L3Addr dst_l3addr(5);
  SendInterfaceImplementation<L3Addr, L2Addr> l3_send_policy(src_l3addr);
  ArdNetworkLayerThree l3(&mem_pool, &sys_int_mock, &eventManager,
                           &l3_send_policy, src_l3addr);

  // Mock the application south interface
  ArdSouthInterfaceMock southInterfaceMock;
  l3.setUpperLayer(&southInterfaceMock);
  
  // Use a fake North interface
  ArdFakeNorthInterface<L2Addr> f_north_int;
  l3_send_policy.setLowerLayer(&f_north_int);
  f_north_int.setUpperLayer(&l3);

  PktBufPtr p1_payload = mem_pool.AllocateSlot();
  p1_payload->data[0] = 14;
  p1_payload->data[1] = 15;
  p1_payload->data[2] = 9;
  p1_payload->curr_size = 3;

  // We have to use a reference packet buffer with the same content as the
  // payload one because we cannot move the original payload twice.
  ArdPktBuffer p1_payload_ref;
  p1_payload_ref.data[0] = 14;
  p1_payload_ref.data[1] = 15;
  p1_payload_ref.data[2] = 9;
  p1_payload_ref.curr_size = 3;
  EXPECT_CALL(southInterfaceMock,
              dataHandlingDone(IsPktBuffEq(p1_payload_ref), true));
  
  ArdPktBuffer p1;
  p1.data[0] = 0;
  p1.data[1] = 3;
  p1.data[2] = 0;
  p1.data[3] = 5;
  p1.data[4] = 8;
  p1.data[5] = 14;
  p1.data[6] = 15;
  p1.data[7] = 9;
  p1.curr_size = 8;
  l3.sendRequest(ard_move(p1_payload), dst_l3addr);

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 1);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(p1));
}

TEST(NetworkLayerThreeTest, OnDataReceived) {

  ArdMemPool<ArdPktBuffer> mem_pool;

  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L3Addr src_l3addr(3);
  L3Addr dst_l3addr(5);
  SendInterfaceImplementation<L3Addr, L2Addr> l3_send_policy(dst_l3addr);
  ArdNetworkLayerThree l3(&mem_pool, &sys_int_mock, &eventManager,
                           &l3_send_policy, dst_l3addr);

  // Mock the application south interface
  ArdSouthInterfaceMock southInterfaceMock;
  l3.setUpperLayer(&southInterfaceMock);
  
  // Use a fake North interface
  ArdFakeNorthInterface<L2Addr> f_north_int;
  l3_send_policy.setLowerLayer(&f_north_int);
  f_north_int.setUpperLayer(&l3);

  ArdPktBuffer p1_ref_payload;
  p1_ref_payload.data[0] = 14;
  p1_ref_payload.data[1] = 15;
  p1_ref_payload.data[2] = 9;
  p1_ref_payload.curr_size = 3;

  PktBufPtr p1 = mem_pool.AllocateSlot();
  p1->data[0] = 0;
  p1->data[1] = 3;
  p1->data[2] = 0;
  p1->data[3] = 5;
  p1->data[4] = 8;
  p1->data[5] = 14;
  p1->data[6] = 15;
  p1->data[7] = 9;
  p1->curr_size = 8;

  L3Addr l3_src_addr(3);
  AnyAddr src_addr(l3_src_addr);
  EXPECT_CALL(southInterfaceMock,
              onDataReceived(IsPktBuffEq(p1_ref_payload), src_addr, _));
  L2Addr l2_addr;
  AnyAddr l2_any_addr(l2_addr);
  l3.onDataReceived(ard_move(p1), l2_any_addr, 0);
}

TEST(NetworkLayerThreeTest, RoutingToLeft) {

  ArdMemPool<ArdPktBuffer> mem_pool;

  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L3Addr src_l3addr(3);
  L3Addr dst_l3addr(514);
  L3Addr left_l3addr(515);
  L3Addr right_l3addr(259);
  SendInterfaceImplementation<L3Addr, L2Addr> l3_send_policy_left(left_l3addr);
  SendInterfaceImplementation<L3Addr, L2Addr> l3_send_policy_right(right_l3addr);
  ArdNetworkRoutingLayerThree l3(&mem_pool, &sys_int_mock, &eventManager,
                           &l3_send_policy_left, &l3_send_policy_right, left_l3addr,
                           right_l3addr);
  
  // Use a fake North interface
  ArdFakeNorthInterface<L2Addr> f_north_int_left;
  l3_send_policy_left.setLowerLayer(&f_north_int_left);
  f_north_int_left.setUpperLayer(&l3);

  // Use a fake North interface
  ArdFakeNorthInterface<L2Addr> f_north_int_right;
  l3_send_policy_right.setLowerLayer(&f_north_int_right);
  f_north_int_right.setUpperLayer(&l3);

  PktBufPtr p1 = mem_pool.AllocateSlot();
  p1->data[0] = 0;
  p1->data[1] = 3;
  p1->data[2] = 2;
  p1->data[3] = 2;
  p1->data[4] = 8;
  p1->data[5] = 14;
  p1->data[6] = 15;
  p1->data[7] = 9;
  p1->curr_size = 8;

  ArdPktBuffer p1_ref;
  p1_ref.data[0] = 0;
  p1_ref.data[1] = 3;
  p1_ref.data[2] = 2;
  p1_ref.data[3] = 2;
  p1_ref.data[4] = 8;
  p1_ref.data[5] = 14;
  p1_ref.data[6] = 15;
  p1_ref.data[7] = 9;
  p1_ref.curr_size = 8;

  L2Addr l2_addr;
  AnyAddr l2_any_addr(l2_addr);
  l3.onDataReceived(ard_move(p1), l2_any_addr, 0);  
  EXPECT_EQ(f_north_int_left.m_pkt_list.size(), 1);
  EXPECT_THAT(f_north_int_left.m_pkt_list, ElementsAre(p1_ref));
}

TEST(NetworkLayerThreeTest, RoutingToRight) {

  ArdMemPool<ArdPktBuffer> mem_pool;

  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L3Addr src_l3addr(3);
  L3Addr dst_l3addr(5);
  L3Addr left_l3addr(515);
  L3Addr right_l3addr(259);
  SendInterfaceImplementation<L3Addr, L2Addr> l3_send_policy_left(left_l3addr);
  SendInterfaceImplementation<L3Addr, L2Addr> l3_send_policy_right(right_l3addr);
  ArdNetworkRoutingLayerThree l3(&mem_pool, &sys_int_mock, &eventManager,
                           &l3_send_policy_left, &l3_send_policy_right, left_l3addr,
                           right_l3addr);
  
  // Use a fake North interface
  ArdFakeNorthInterface<L2Addr> f_north_int_left;
  l3_send_policy_left.setLowerLayer(&f_north_int_left);
  f_north_int_left.setUpperLayer(&l3);

  // Use a fake North interface
  ArdFakeNorthInterface<L2Addr> f_north_int_right;
  l3_send_policy_right.setLowerLayer(&f_north_int_right);
  f_north_int_right.setUpperLayer(&l3);

  PktBufPtr p1 = mem_pool.AllocateSlot();
  p1->data[0] = 0;
  p1->data[1] = 3;
  p1->data[2] = 0;
  p1->data[3] = 5;
  p1->data[4] = 8;
  p1->data[5] = 14;
  p1->data[6] = 15;
  p1->data[7] = 9;
  p1->curr_size = 8;

  ArdPktBuffer p1_ref;
  p1_ref.data[0] = 0;
  p1_ref.data[1] = 3;
  p1_ref.data[2] = 0;
  p1_ref.data[3] = 5;
  p1_ref.data[4] = 8;
  p1_ref.data[5] = 14;
  p1_ref.data[6] = 15;
  p1_ref.data[7] = 9;
  p1_ref.curr_size = 8;

  L2Addr l2_addr;
  AnyAddr l2_any_addr(l2_addr);
  l3.onDataReceived(ard_move(p1), l2_any_addr, 0);  
  EXPECT_EQ(f_north_int_right.m_pkt_list.size(), 1);
  EXPECT_THAT(f_north_int_right.m_pkt_list, ElementsAre(p1_ref));
}
