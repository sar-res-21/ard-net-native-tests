#include "ard_event_manager.h"
#include "ard_fake_network_interfaces.h"
#include "ard_mempool.h"
#include "ard_network_address.h"
#include "ard_network_layer_interface_mock.h"
#include "ard_network_layer_two.h"
#include "ard_sys_interface_mock.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using ::testing::_;
using ::testing::Each;
using ::testing::ElementsAre;
using ::testing::Invoke;
using ::testing::NiceMock;

MATCHER_P(IsPktBuffEq, ref_pk, "")
{
  if (*arg == ref_pk)
    return true;

  *result_listener << "Packet buffers are different\n";
  *result_listener << "The test was expecting this packet:\n";
  *result_listener << ref_pk.prettyPrint().str();
  *result_listener << "But your implementation sent this one:\n";
  *result_listener << arg->prettyPrint().str();
  return false;
}

struct FakeMillis
{
  FakeMillis() : i(4500) {}

  unsigned long fakeMillis()
  {
    if (i < 6000)
    {
      i += 250;
    }
    return i;
  }
  unsigned long i;
};

TEST(NetworkLayerTwoMultiNodeTest, SendRequestTWoNodes)
{
  ArdMemPool<ArdPktBuffer> mem_pool;
  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L2Addr local_l2addr(4);
  L2Addr dst1_l2addr(7);
  L2Addr dst2_l2addr(9);
  SendInterfaceImplementation<L2Addr, L1Addr> l2_send_policy(local_l2addr);
  ArdNetworkLayerTwoAck l2(&mem_pool, &sys_int_mock, &eventManager,
                           &l2_send_policy, local_l2addr);

  // Mock the layer 3 south interface
  ArdSouthInterfaceMock southInterfaceMock;
  l2.setUpperLayer(&southInterfaceMock);

  // Use a fake North interface
  ArdFakeNorthInterface<L1Addr> f_north_int;
  l2_send_policy.setLowerLayer(&f_north_int);
  f_north_int.setUpperLayer(&l2);

  // Payload of first packet for dst1
  PktBufPtr p1_dst1 = mem_pool.AllocateSlot();
  p1_dst1->data[0] = 2;
  p1_dst1->data[1] = 5;
  p1_dst1->data[2] = 3;
  p1_dst1->curr_size = 3;
  // Reference packet for p1_dst1
  ArdPktBuffer ref_p1_dst1;
  ref_p1_dst1.data[0] = 7;
  ref_p1_dst1.data[1] = 4;
  ref_p1_dst1.data[2] = 1;
  ref_p1_dst1.data[3] = 0;
  ref_p1_dst1.data[4] = 3;
  ref_p1_dst1.data[5] = 2;
  ref_p1_dst1.data[6] = 5;
  ref_p1_dst1.data[7] = 3;
  ref_p1_dst1.curr_size = 8;

  // Payload of second packet for dst1
  PktBufPtr p2_dst1 = mem_pool.AllocateSlot();
  p2_dst1->data[0] = 8;
  p2_dst1->data[1] = 2;
  p2_dst1->curr_size = 2;
  // Reference packet for p2_dst1
  ArdPktBuffer ref_p2_dst1;
  ref_p2_dst1.data[0] = 7;
  ref_p2_dst1.data[1] = 4;
  ref_p2_dst1.data[2] = 1;
  ref_p2_dst1.data[3] = 1;
  ref_p2_dst1.data[4] = 2;
  ref_p2_dst1.data[5] = 8;
  ref_p2_dst1.data[6] = 2;
  ref_p2_dst1.curr_size = 7;

  // Payload of first packet for dst2
  PktBufPtr p1_dst2 = mem_pool.AllocateSlot();
  p1_dst2->data[0] = 10;
  p1_dst2->data[1] = 11;
  p1_dst2->data[2] = 12;
  p1_dst2->curr_size = 3;
  // Reference packet for p1_dst2
  ArdPktBuffer ref_p1_dst2;
  ref_p1_dst2.data[0] = 9;
  ref_p1_dst2.data[1] = 4;
  ref_p1_dst2.data[2] = 1;
  ref_p1_dst2.data[3] = 0;
  ref_p1_dst2.data[4] = 3;
  ref_p1_dst2.data[5] = 10;
  ref_p1_dst2.data[6] = 11;
  ref_p1_dst2.data[7] = 12;
  ref_p1_dst2.curr_size = 8;

  // ACK for the first packet from dst1
  PktBufPtr ack_p1_dst1 = mem_pool.AllocateSlot();
  ack_p1_dst1->data[0] = 4;
  ack_p1_dst1->data[1] = 7;
  ack_p1_dst1->data[2] = 0;
  ack_p1_dst1->data[3] = 0;
  ack_p1_dst1->data[4] = 0;
  ack_p1_dst1->curr_size = 5;

  // ACK for the second packet from dst1
  PktBufPtr ack_p2_dst1 = mem_pool.AllocateSlot();
  ack_p2_dst1->data[0] = 4;
  ack_p2_dst1->data[1] = 7;
  ack_p2_dst1->data[2] = 0;
  ack_p2_dst1->data[3] = 1;
  ack_p2_dst1->data[4] = 0;
  ack_p2_dst1->curr_size = 5;

  l2.sendRequest(ard_move(p1_dst1), dst1_l2addr);
  l2.sendRequest(ard_move(p2_dst1), dst1_l2addr);

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 1);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_p1_dst1));

  L1Addr l1_addr;
  AnyAddr l1_any_addr(l1_addr);
  l2.onDataReceived(ard_move(ack_p1_dst1), l1_any_addr, 0);

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 2);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_p1_dst1, ref_p2_dst1));

  l2.sendRequest(ard_move(p1_dst2), dst2_l2addr);
  l2.onDataReceived(ard_move(ack_p2_dst1), l1_any_addr, 0);
  EXPECT_EQ(f_north_int.m_pkt_list.size(), 3);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_p1_dst1, ref_p2_dst1, ref_p1_dst2));
}

TEST(NetworkLayerTwoMultiNodeTest, OnDataReceivedTWoNodes)
{
  ArdMemPool<ArdPktBuffer> mem_pool;
  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L2Addr local_l2addr(4);
  L2Addr src1_l2addr(7);
  L2Addr src2_l2addr(9);
  SendInterfaceImplementation<L2Addr, L1Addr> l2_send_policy(local_l2addr);
  ArdNetworkLayerTwoAck l2(&mem_pool, &sys_int_mock, &eventManager,
                           &l2_send_policy, local_l2addr);

  // Mock the layer 3 south interface
  ArdSouthInterfaceMock southInterfaceMock;
  l2.setUpperLayer(&southInterfaceMock);

  // Use a fake North interface
  ArdFakeNorthInterface<L1Addr> f_north_int;
  l2_send_policy.setLowerLayer(&f_north_int);
  f_north_int.setUpperLayer(&l2);

  // First packet from src1
  PktBufPtr p1_src1 = mem_pool.AllocateSlot();
  p1_src1->data[0] = 4;
  p1_src1->data[1] = 7;
  p1_src1->data[2] = 1;
  p1_src1->data[3] = 0;
  p1_src1->data[4] = 5;
  p1_src1->data[5] = 10;
  p1_src1->data[6] = 11;
  p1_src1->data[7] = 12;
  p1_src1->data[8] = 13;
  p1_src1->data[9] = 14;
  p1_src1->curr_size = 10;
  // Reference payload for p1_src1
  ArdPktBuffer ref_pl1_src1;
  ref_pl1_src1.data[0] = 10;
  ref_pl1_src1.data[1] = 11;
  ref_pl1_src1.data[2] = 12;
  ref_pl1_src1.data[3] = 13;
  ref_pl1_src1.data[4] = 14;
  ref_pl1_src1.curr_size = 5;
  ArdPktBuffer ref_ack1_src1;
  ref_ack1_src1.data[0] = 7;
  ref_ack1_src1.data[1] = 4;
  ref_ack1_src1.data[2] = 0;
  ref_ack1_src1.data[3] = 0;
  ref_ack1_src1.data[4] = 0;
  ref_ack1_src1.curr_size = 5;

  AnyAddr any_l2_src1_addr(src1_l2addr);
  EXPECT_CALL(southInterfaceMock,
              onDataReceived(IsPktBuffEq(ref_pl1_src1), any_l2_src1_addr, _));
  L1Addr l1_addr;
  l2.onDataReceived(ard_move(p1_src1), AnyAddr(l1_addr), 0);

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 1);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_ack1_src1));

  // Second packet from src1
  PktBufPtr p2_src1 = mem_pool.AllocateSlot();
  p2_src1->data[1] = 7;
  p2_src1->data[0] = 4;
  p2_src1->data[2] = 1;
  p2_src1->data[3] = 1;
  p2_src1->data[4] = 3;
  p2_src1->data[5] = 7;
  p2_src1->data[6] = 9;
  p2_src1->data[7] = 8;
  p2_src1->curr_size = 8;
  // Reference payload for p2_src1
  ArdPktBuffer ref_pl2_src1;
  ref_pl2_src1.data[0] = 7;
  ref_pl2_src1.data[1] = 9;
  ref_pl2_src1.data[2] = 8;
  ref_pl2_src1.curr_size = 3;
  ArdPktBuffer ref_ack2_src1;
  ref_ack2_src1.data[0] = 7;
  ref_ack2_src1.data[1] = 4;
  ref_ack2_src1.data[2] = 0;
  ref_ack2_src1.data[3] = 1;
  ref_ack2_src1.data[4] = 0;
  ref_ack2_src1.curr_size = 5;

  EXPECT_CALL(southInterfaceMock,
              onDataReceived(IsPktBuffEq(ref_pl2_src1), any_l2_src1_addr, _));
  l2.onDataReceived(ard_move(p2_src1), AnyAddr(l1_addr), 0);

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 2);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_ack1_src1, ref_ack2_src1));

  // First packet from src2
  PktBufPtr p1_src2 = mem_pool.AllocateSlot();
  p1_src2->data[0] = 4;
  p1_src2->data[1] = 9;
  p1_src2->data[2] = 1;
  p1_src2->data[3] = 0;
  p1_src2->data[4] = 5;
  p1_src2->data[5] = 14;
  p1_src2->data[6] = 15;
  p1_src2->data[7] = 12;
  p1_src2->data[8] = 11;
  p1_src2->data[9] = 14;
  p1_src2->curr_size = 10;
  // Reference payload for p1_src2
  ArdPktBuffer ref_pl1_src2;
  ref_pl1_src2.data[0] = 14;
  ref_pl1_src2.data[1] = 15;
  ref_pl1_src2.data[2] = 12;
  ref_pl1_src2.data[3] = 11;
  ref_pl1_src2.data[4] = 14;
  ref_pl1_src2.curr_size = 5;
  ArdPktBuffer ref_ack1_src2;
  ref_ack1_src2.data[0] = 9;
  ref_ack1_src2.data[1] = 4;
  ref_ack1_src2.data[2] = 0;
  ref_ack1_src2.data[3] = 0;
  ref_ack1_src2.data[4] = 0;
  ref_ack1_src2.curr_size = 5;

  AnyAddr any_l2_src2_addr(src2_l2addr);
  EXPECT_CALL(southInterfaceMock,
              onDataReceived(IsPktBuffEq(ref_pl1_src2), any_l2_src2_addr, _));
  l2.onDataReceived(ard_move(p1_src2), AnyAddr(l1_addr), 0);

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 3);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_ack1_src1, ref_ack2_src1, ref_ack1_src2));
}