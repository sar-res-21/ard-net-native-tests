
#include "ard_event_manager.h"
#include "ard_fake_network_interfaces.h"
#include "ard_mempool.h"
#include "ard_network_address.h"
#include "ard_network_layer_interface_mock.h"
#include "ard_network_layer_two.h"
#include "ard_sys_interface_mock.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using ::testing::_;
using ::testing::Each;
using ::testing::ElementsAre;
using ::testing::NiceMock;
using ::testing::Invoke;

MATCHER_P(IsPktBuffEq, ref_pk, "") {
  if (*arg == ref_pk)
    return true;

  *result_listener << "Packet buffers are different\n";
  *result_listener << "The test was expecting this packet:\n";
  *result_listener << ref_pk.prettyPrint().str();
  *result_listener << "But your implementation sent this one:\n";
  *result_listener << arg->prettyPrint().str();
  return false;
}


struct FakeMillis
{
  FakeMillis() : i(4500){}

  unsigned long fakeMillis(  )
  {
    if (i<6000){
      i+=250;
    }
    return i;
  }
  unsigned long i;
};

TEST(NetworkLayerTwoTest, SimpleSendRequest) {

  ArdMemPool<ArdPktBuffer> mem_pool;

  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L2Addr src_l2addr(3);
  L2Addr dst_l2addr(5);
  SendInterfaceImplementation<L2Addr, L1Addr> l2_send_policy(src_l2addr);
  ArdNetworkLayerTwoAck l2(&mem_pool, &sys_int_mock, &eventManager,
                           &l2_send_policy, src_l2addr);

  // Mock the layer 3 south interface
  ArdSouthInterfaceMock southInterfaceMock;
  l2.setUpperLayer(&southInterfaceMock);
  
  // Use a fake North interface
  ArdFakeNorthInterface<L1Addr> f_north_int;
  l2_send_policy.setLowerLayer(&f_north_int);
  f_north_int.setUpperLayer(&l2);

  PktBufPtr p1 = mem_pool.AllocateSlot();
  p1->data[0] = 10;
  p1->data[1] = 11;
  p1->curr_size = 2;

  ArdPktBuffer ref_p;
  ref_p.data[0] = 5;
  ref_p.data[1] = 3;
  ref_p.data[2] = 1;
  ref_p.data[3] = 0;
  ref_p.data[4] = 2;
  ref_p.data[5] = 10;
  ref_p.data[6] = 11;
  ref_p.curr_size = 7;
  l2.sendRequest(ard_move(p1), dst_l2addr);

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 1);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_p));
}

TEST(NetworkLayerTwoTest, SendRequestOneTimeout) {

  ArdMemPool<ArdPktBuffer> mem_pool;
  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;

  FakeMillis fake_time;
  ON_CALL(sys_int_mock, getMillis()).WillByDefault(Invoke(&fake_time, &FakeMillis::fakeMillis));

  ArdEventManager eventManager(&sys_int_mock);
  L2Addr src_l2addr(3);
  L2Addr dst_l2addr(5);
  SendInterfaceImplementation<L2Addr, L1Addr> l2_send_policy(src_l2addr);
  ArdNetworkLayerTwoAck l2(&mem_pool, &sys_int_mock, &eventManager,
                           &l2_send_policy, src_l2addr);

  // Mock the layer 3 south interface
  ArdSouthInterfaceMock southInterfaceMock;
  l2.setUpperLayer(&southInterfaceMock);
  
  // Use a fake North interface
  ArdFakeNorthInterface<L1Addr> f_north_int;
  l2_send_policy.setLowerLayer(&f_north_int);
  f_north_int.setUpperLayer(&l2);

  PktBufPtr p1 = mem_pool.AllocateSlot();
  p1->data[0] = 10;
  p1->data[1] = 11;
  p1->curr_size = 2;

  ArdPktBuffer ref_p;
  ref_p.data[0] = 5;
  ref_p.data[1] = 3;
  ref_p.data[2] = 1;
  ref_p.data[3] = 0;
  ref_p.data[4] = 2;
  ref_p.data[5] = 10;
  ref_p.data[6] = 11;
  ref_p.curr_size = 7;
  l2.sendRequest(ard_move(p1), dst_l2addr);

  eventManager.loopIteration();
  eventManager.loopIteration();
  eventManager.loopIteration();
  eventManager.loopIteration();
  eventManager.loopIteration();
  eventManager.loopIteration();

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 2);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_p, ref_p));
}

TEST(NetworkLayerTwoTest, onDataReceived) {
  ArdMemPool<ArdPktBuffer> mem_pool;
  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L2Addr src_l2addr(3);
  L2Addr dst_l2addr(5);
  SendInterfaceImplementation<L2Addr, L1Addr> l2_send_policy(dst_l2addr);
  ArdNetworkLayerTwoAck l2(&mem_pool, &sys_int_mock, &eventManager,
                           &l2_send_policy, dst_l2addr);

  // Mock the layer 3 south interface
  ArdSouthInterfaceMock southInterfaceMock;
  l2.setUpperLayer(&southInterfaceMock);

  // Use a fake North interface
  ArdFakeNorthInterface<L1Addr> f_north_int;
  l2_send_policy.setLowerLayer(&f_north_int);
  f_north_int.setUpperLayer(&l2);


  PktBufPtr p1 = mem_pool.AllocateSlot();

  p1->data[0] = 5;
  p1->data[1] = 3;
  p1->data[2] = 1;
  p1->data[3] = 0;
  p1->data[4] = 2;
  p1->data[5] = 10;
  p1->data[6] = 11;
  p1->curr_size = 7;

  ArdPktBuffer ref_p;
  ref_p.data[0] = 10;
  ref_p.data[1] = 11;
  ref_p.curr_size = 2;

  L2Addr l2_src_addr(3);
  AnyAddr src_addr(l2_src_addr);
  EXPECT_CALL(southInterfaceMock,
              onDataReceived(IsPktBuffEq(ref_p), src_addr, _));
  L1Addr l1_addr;
  AnyAddr l1_any_addr(l1_addr);
  l2.onDataReceived(ard_move(p1), l1_any_addr, 0);
}


TEST(NetworkLayerTwoTest, onDataReceivedPlusACK) {
  ArdMemPool<ArdPktBuffer> mem_pool;
  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L2Addr src_l2addr(3);
  L2Addr dst_l2addr(5);
  SendInterfaceImplementation<L2Addr, L1Addr> l2_send_policy(dst_l2addr);
  ArdNetworkLayerTwoAck l2(&mem_pool, &sys_int_mock, &eventManager,
                           &l2_send_policy, dst_l2addr);

  // Mock the layer 3 south interface
  ArdSouthInterfaceMock southInterfaceMock;
  l2.setUpperLayer(&southInterfaceMock);

  // Use a fake North interface
  ArdFakeNorthInterface<L1Addr> f_north_int;
  l2_send_policy.setLowerLayer(&f_north_int);
  f_north_int.setUpperLayer(&l2);


  PktBufPtr p1 = mem_pool.AllocateSlot();

  p1->data[0] = 5;
  p1->data[1] = 3;
  p1->data[2] = 1;
  p1->data[3] = 0;
  p1->data[4] = 2;
  p1->data[5] = 10;
  p1->data[6] = 11;
  p1->curr_size = 7;

  ArdPktBuffer ref_p;
  ref_p.data[0] = 10;
  ref_p.data[1] = 11;
  ref_p.curr_size = 2;

  ArdPktBuffer ref_ack;
  ref_ack.data[0] = 3;
  ref_ack.data[1] = 5;
  ref_ack.data[2] = 0;
  ref_ack.data[3] = 0;
  ref_ack.data[4] = 0;
  ref_ack.curr_size = 5;

  L2Addr l2_src_addr(3);
  AnyAddr any_l2_src_addr(l2_src_addr);
  EXPECT_CALL(southInterfaceMock,
              onDataReceived(IsPktBuffEq(ref_p), any_l2_src_addr, _));
  L1Addr l1_addr;
  l2.onDataReceived(ard_move(p1), AnyAddr(l1_addr), 0);

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 1);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_ack));
}

TEST(NetworkLayerTwoTest, SimpleSendRequestPlusSendCompleted) {

  ArdMemPool<ArdPktBuffer> mem_pool;

  NiceMock<ArdSysInterfaceImplMock> sys_int_mock;
  ArdEventManager eventManager(&sys_int_mock);
  L2Addr src_l2addr(3);
  L2Addr dst_l2addr(5);
  SendInterfaceImplementation<L2Addr, L1Addr> l2_send_policy(src_l2addr);
  ArdNetworkLayerTwoAck l2(&mem_pool, &sys_int_mock, &eventManager,
                           &l2_send_policy, src_l2addr);

  // Use a fake North interface
  ArdFakeNorthInterface<L1Addr> f_north_int;
  l2_send_policy.setLowerLayer(&f_north_int);
  f_north_int.setUpperLayer(&l2);

  // Mock the layer 3 south interface
  ArdSouthInterfaceMock southInterfaceMock;
  l2.setUpperLayer(&southInterfaceMock);

  PktBufPtr p1 = mem_pool.AllocateSlot();
  p1->data[0] = 10;
  p1->data[1] = 11;
  p1->curr_size = 2;

  ArdPktBuffer p2;
  p2.data[0] = 10;
  p2.data[1] = 11;
  p2.curr_size = 2;
  EXPECT_CALL(southInterfaceMock,
              dataHandlingDone(IsPktBuffEq(p2), true));

  ArdPktBuffer ref_p;
  ref_p.data[0] = 5;
  ref_p.data[1] = 3;
  ref_p.data[2] = 1;
  ref_p.data[3] = 0;
  ref_p.data[4] = 2;
  ref_p.data[5] = 10;
  ref_p.data[6] = 11;
  ref_p.curr_size = 7;
  l2.sendRequest(ard_move(p1), dst_l2addr);

  EXPECT_EQ(f_north_int.m_pkt_list.size(), 1);
  EXPECT_THAT(f_north_int.m_pkt_list, ElementsAre(ref_p));

}